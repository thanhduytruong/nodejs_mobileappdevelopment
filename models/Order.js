const { Model } = require('objection');

class Order extends Model {
  static get tableName() {
    return 'orders';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['date', 'totalAmount', 'ownerId'],
      properties: {
        id: { type: 'integer' },
        date: { type: 'string' },
        totalAmount: { type: 'number' },
        ownerId: { type: 'integer' },
      },
    };
  }
}

module.exports = Order;
