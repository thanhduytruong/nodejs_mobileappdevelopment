const Product = require('../models/Product');
const CustomError = require('../errors/CustomError');

async function getList(userId) {
  const products = await Product.query()
    .select('products.*')
    .where('ownerId', userId);

  return products;
}

async function getAll() {
  const products = await Product.query().select('products.*');

  return products;
}

async function get(userId, productId) {
  if (!productId) {
    throw new CustomError(400, 'productId is required');
  }

  const product = await Product.query()
    .findById(productId)
    .where('ownerId', userId);

  if (!product) {
    return {};
  }

  return product;
}

async function insert(userId, product) {
  const insertedProduct = product;
  insertedProduct.ownerId = userId;
  const dbProduct = await Product.query().insert(insertedProduct);
  return dbProduct;
}

async function update(productId, product) {
  if (!productId) {
    throw new CustomError(400, 'productId is required');
  }
  const dbProduct = await Product.query().findById(productId).patch(product);
  return dbProduct;
}

module.exports = { getList, getAll, get, insert, update };
