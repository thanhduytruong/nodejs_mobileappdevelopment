function getErrorDefaults(code) {
  switch (code) {
    case 400:
      return { code, message: 'Bad request' };
    case 401:
      return { code, message: 'Unauthorized' };
    case 403:
      return { code, message: 'Forbidden' };
    case 404:
      return { code, message: 'Not Found' };
    case 405:
      return { code, message: 'Method Not Allowed' };
    case 408:
      return { code, message: 'Request Timeout' };
    case 422:
      return { code, message: 'Unprocessable entity' };
    default:
      return { code: 500, message: 'Internal Server Error' };
  }
}

module.exports = getErrorDefaults;
