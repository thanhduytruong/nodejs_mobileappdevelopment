exports.up = (knex) => {
  return knex.schema.createTable('users', (table) => {
    table.increments('id').primary();
    table.string('email', 45).unique().notNullable();
    table.string('password').notNullable();
    table.string('name', 45).notNullable();
    table.string('phone', 15).notNullable();
    table.string('token');
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable('users');
};
