const crypto = require('crypto');
const bcryptJS = require('bcryptjs');

const sha256 = (string, key) => {
  const hmac = crypto.createHmac('sha256', key);
  const data = hmac.update(string);
  const genHMAC = data.digest('hex');

  return genHMAC;
};

const bcrypt = async (string) => {
  const salt = await bcryptJS.genSaltSync(10);
  const hash = await bcryptJS.hashSync(string, salt);
  return hash;
};

const bcryptCompare = async (string, hash) => {
  const isMatched = await bcryptJS.compare(string, hash);
  return isMatched;
};

// async function test() {
//   const hash = await bcrypt('abc');
//   const result = await bcryptCompare('abcd', hash);
//   console.log(result);
// }

// test();
module.exports = { sha256, bcrypt, bcryptCompare };
