exports.up = (knex) => {
  return knex.schema.createTable('orderDetails', (table) => {
    table.increments('id').primary();
    table.string('title', 45).notNullable();
    table.integer('price').unsigned().notNullable();
    table.string('description').notNullable();
    table.string('imageUrl').notNullable();
    table.integer('quantity').unsigned().notNullable();
    table
      .integer('orderId')
      .unsigned()
      .references('id')
      .inTable('orders')
      .onDelete('CASCADE');
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable('orderDetails');
};
