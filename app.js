const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const Knex = require('knex');
const { Model } = require('objection');
require('dotenv').config();
const {
  handleCommonError,
  handle405Error,
} = require('./middlewares/errorHandlers');
const dbConfig = require('./configs/db');
// const priceJob = require('./schedules/schedule');

const knex = Knex(dbConfig.development);
Model.knex(knex);

const app = express();
const port = process.env.PORT || 2569;

app.use(cors());
app.use(morgan(':method :url :status'));
app.use(express.json());

app.use('/', require('./apis/auth.api'));
app.use('/products', require('./apis/product.api'));
app.use('/orders', require('./apis/order.api'));

app.use(handle405Error);
app.use(handleCommonError);
// priceJob.start();

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is running at port ${port}`);
});
