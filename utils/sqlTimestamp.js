const now = () => {
  let date = new Date();

  date = new Date(Math.round(date.getTime() + 420 * 60000));

  return date.toISOString().slice(0, 19).replace('T', ' ');
};

module.exports = { now };
