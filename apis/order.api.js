const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async.middleware');
const auth = require('../middlewares/auth.middleware');
const { createOrder, getOrders } = require('../controllers/order.controller');

router.get('/', auth, asyncMiddleware(getOrders));
router.post('/', auth, asyncMiddleware(createOrder));

module.exports = router;
