const { Model } = require('objection');

class Product extends Model {
  static get tableName() {
    return 'products';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['description', 'imageUrl', 'price', 'title', 'ownerId'],
      properties: {
        id: { type: 'integer' },
        description: { type: 'string' },
        imageUrl: { type: 'string' },
        price: { type: 'number' },
        title: { type: 'string' },
        ownerId: { type: 'integer' },
      },
    };
  }
}

module.exports = Product;
