const { Model } = require('objection');

class User extends Model {
  static get tableName() {
    return 'users';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        id: { type: 'integer' },
        email: { type: 'string', minLength: 1, maxLength: 45 },
        password: { type: 'string', maxLength: 255 },
        name: { type: 'string', maxLength: 45 },
        phone: { type: 'string', maxLength: 15 },
      },
    };
  }
}
//   //   static get relationMappings() {
//   //     return {
//   //       comments: {
//   //         relation: Model.HasManyRelation,

//   //         modelClass: Comment,
//   //         join: {
//   //           from: 'ideas.id',
//   //           to: 'comments.ideas_id',
//   //         },
//   //       },
//   //     };
//   //   }
// }

// // // findById(700);
// // insert().catch(error => {
// //   console.log(error);
// // });

module.exports = User;
