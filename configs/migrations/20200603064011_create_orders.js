exports.up = (knex) => {
  return knex.schema.createTable('orders', (table) => {
    table.increments('id').primary();
    table.timestamp('date').notNullable();
    table.integer('totalAmount').unsigned().notNullable();
    table
      .integer('ownerId')
      .unsigned()
      .references('id')
      .inTable('users')
      .onDelete('CASCADE');
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable('orders');
};
