const service = require('../services/product.service');

async function createProduct(req, res) {
  const product = await service.insert(req.userId, req.body);
  res.status(200).send({ product });
}

async function getProducts(req, res) {
  const products = await service.getList(req.userId);
  res.status(200).send({ products });
}

async function getAllProducts(req, res) {
  const products = await service.getAll();
  res.status(200).send({ products });
}

async function getProduct(req, res) {
  const product = await service.get(req.userId, req.params.id);
  res.status(200).send({ product });
}

async function updateProduct(req, res) {
  const product = await service.update(req.params.id, req.body);
  res.status(200).send({ product });
}

module.exports = {
  createProduct,
  getProducts,
  getAllProducts,
  getProduct,
  updateProduct,
};
