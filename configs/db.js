require('dotenv').config({ path: '../../.env' });

const dbConfig = {
  development: {
    client: process.env.CLIENT || 'mysql',
    connection: {
      host: process.env.HOST || '127.0.0.1',
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD || 'password',
      database: process.env.DB_NAME || 'expo',
      typeCast(field, next) {
        // Convert 1 to true, 0 to false, and leave null alone
        if (field.type === 'TINY' && field.length === 1) {
          const value = field.string();
          return value ? value === '1' : null;
        }
        return next();
      },
    },
  },
};

module.exports = dbConfig;
