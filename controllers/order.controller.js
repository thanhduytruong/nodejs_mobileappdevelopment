const service = require('../services/order.service');

async function createOrder(req, res) {
  const order = await service.insert(req.userId, req.body.cartItems);
  res.status(200).send({ order });
}

async function getOrders(req, res) {
  const orders = await service.getList(req.userId);
  res.status(200).send({ orders });
}

module.exports = { createOrder, getOrders };
