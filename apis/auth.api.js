const router = require('express').Router();
const auth = require('../middlewares/auth.middleware');
const asyncMiddleware = require('../middlewares/async.middleware');
const {
  register,
  login,
  update,
  changePassword,
  getInfo,
} = require('../controllers/auth.controller');

router.post('/register', asyncMiddleware(register));
router.post('/login', asyncMiddleware(login));
router.put('/user', auth, asyncMiddleware(update));
router.put('/user/change_password', auth, asyncMiddleware(changePassword));
router.get('/user/info', auth, asyncMiddleware(getInfo));

module.exports = router;
