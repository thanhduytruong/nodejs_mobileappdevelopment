exports.up = (knex) => {
  return knex.schema.createTable('products', (table) => {
    table.increments('id').primary();
    table.string('title', 45).notNullable();
    table.integer('price').unsigned().notNullable();
    table.string('description').notNullable();
    table.string('imageUrl').notNullable();
    table
      .integer('ownerId')
      .unsigned()
      .references('id')
      .inTable('users')
      .onDelete('CASCADE');
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable('products');
};
